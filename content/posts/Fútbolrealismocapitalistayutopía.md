---
title: "Fútbol, realismo capitalista y utopía de Mark Fisher"
date: 2023-03-22
draft: false
author: "x3r0x"
tags:  ["futbol", "capitalismo", "Fisher"]
images: ["/images/america.jpeg"]
---

![BarricadaAntifa](/images/america.jpeg)
---
El fútbol es el deporte más popular del mundo, capaz de despertar pasiones, emociones y sentimientos en millones de personas. Sin embargo, el fútbol también es una expresión del realismo capitalista, el concepto acuñado por el teórico británico Mark Fisher para describir [“la idea muy difundida de que el capitalismo no solo es el único sistema económico viable, sino que es imposible incluso imaginarle una alternativa”](https://www.youtube.com/watch?v=9qutq579pi8). En este articulo, se analiza cómo el fútbol se ha visto afectado por el realismo capitalista y cómo se podría pensar en una utopía futbolística inspirada en las ideas de Fisher.

## Realismo capitalista en el futbol

El realismo capitalista se manifiesta en el fútbol de varias formas. Por un lado, el fútbol se ha convertido en un negocio globalizado y lucrativo, dominado por unos pocos clubes poderosos que acaparan los recursos, los títulos y los mejores jugadores. La competencia se reduce a una lógica mercantil, donde el éxito se mide por el dinero y no por el mérito deportivo. Los aficionados se convierten en consumidores, sometidos a las estrategias de marketing y a las fluctuaciones del mercado. Por otro lado, el fútbol se ha vuelto una forma de entretenimiento masivo y alienante, que ofrece una vía de escape a las frustraciones y los problemas de la vida cotidiana. El fútbol genera una ilusión de pertenencia y de identidad colectiva, que oculta las desigualdades y las injusticias sociales. El fútbol también produce una sensación de fatalismo y resignación, al aceptar las reglas del juego como naturales e inevitables.

Un ejemplo de cómo el realismo capitalista afecta al fútbol es el caso de Colombia. El fútbol colombiano ha sufrido una crisis estructural desde hace décadas, marcada por la violencia, la corrupción, la falta de inversión y la pérdida de competitividad. Los clubes colombianos han quedado rezagados frente a los gigantes del continente y del mundo, dependiendo de la venta de sus jugadores para subsistir. Los aficionados colombianos han perdido la ilusión por sus equipos locales y han optado por seguir a los clubes europeos o a la selección nacional. El fútbol colombiano se ha convertido en un reflejo de la realidad social del país: desigualdad, violencia, corrupción e impunidad.

¿Es posible imaginar un fútbol diferente, más allá del realismo capitalista? ¿Es posible concebir una utopía futbolística, donde el fútbol sea una práctica liberadora y transformadora? [Mark Fisher nos invita a pensar en la utopía como “un horizonte hacia el cual nos movemos”](https://dialnet.unirioja.es/descarga/articulo/7958138.pdf), una forma de cuestionar lo existente y de proponer lo posible. Una utopía futbolística podría implicar varias cosas: democratizar el fútbol, dando voz y participación a los aficionados, los jugadores y los entrenadores; diversificar el fútbol, reconociendo la pluralidad de formas de jugar y de disfrutar del deporte; desmercantilizar el fútbol, limitando la influencia del dinero y recuperando el valor social y cultural del juego; desalienar el fútbol, conectándolo con las realidades y las aspiraciones de las comunidades; desafiar el fútbol, creando espacios de crítica y de resistencia al orden establecido.

## Utopia futbolistica

El Atlético Nacional de Medellín es un club que ha logrado destacarse en los últimos años por su propuesta deportiva basada en la formación de talentos locales, el juego ofensivo y vistoso, y el respeto por los valores del fútbol. [El Atlético Nacional ha sido campeón de la Copa Libertadores en dos ocasiones (1989 y 2016), de la Copa Merconorte en dos ocasiones (1998 y 2000), de la Copa Interamericana en dos ocasiones (1990 y 1997), de la Recopa Sudamericana en una ocasión (2017) y de la liga colombiana en 17 ocasiones.](https://en.wikipedia.org/wiki/Atl%C3%A9tico_Nacional) Además, el Atlético Nacional ha sido reconocido por su solidaridad y su compromiso social, como lo demostró al pedir que se le otorgara el título de la Copa Sudamericana 2016 al Chapecoense, tras el trágico accidente aéreo que sufrió el equipo brasileño cuando viajaba a Medellín para disputar la final.

El Atlético Nacional podría considerarse como un ejemplo de utopía futbolística, al menos parcialmente, ya que representa una forma de hacer fútbol diferente a la dominante por el realismo capitalista. El Atlético Nacional ha demostrado que se puede competir con éxito sin renunciar a la identidad, a la calidad y a la ética. El Atlético Nacional ha sabido conectar con su afición y con su entorno, generando un sentido de pertenencia y de orgullo. El Atlético Nacional ha desafiado al orden establecido, enfrentándose a equipos más poderosos económicamente y superándolos con talento y trabajo. El Atlético Nacional ha sido una fuente de inspiración y de esperanza para muchos aficionados al fútbol que sueñan con un fútbol más justo y más humano.

## Conclusión

El fútbol es un fenómeno cultural que refleja las contradicciones y los desafíos de nuestra sociedad. El realismo capitalista ha impregnado al fútbol con su lógica mercantilista, alienante y fatalista, pero también existen alternativas que cuestionan y proponen otras formas de entender y practicar el deporte. Mark Fisher nos invita a pensar en la utopía como una forma de resistir al realismo capitalista y de imaginar otros mundos posibles. El fútbol puede ser una herramienta para construir colectivamente un mundo habitable, donde el juego sea una expresión de libertad, creatividad y solidaridad.


## Colectivos Antifas de futbol:

[Barricada 1927 Antifascista /norte](https://www.facebook.com/Barricada1927)

[Coordinadora Antifascista Colombia](https://www.facebook.com/161Colombia)

[Hinchadas Antifascistas Colombia](https://www.facebook.com/Hinchadas.Antifa.Col)

[Antifa CAN](https://www.facebook.com/AntifaCAN)

[Antifa Medallo](https://www.facebook.com/profile.php?id=100067711172460)

[Por la Banda Izquierda](https://www.facebook.com/porlabandaizquierda)

[Futboleras: Pasión femenina](https://www.facebook.com/Futboleras.pasionfemenina1998)

[Fútbola- Fútbol, Mujeres, Conciencia y Sociedad](https://www.facebook.com/cfutbola)

[Coordinadora Futbolera y Feminista Colombia](https://www.facebook.com/profile.php?id=100076426216426)

[La Banda Del Indio](https://www.facebook.com/LaBandaDelIndioCucuta)

[La famoxa banda oficial](https://www.facebook.com/lafamoxabanda1998)

### Referencias

Fisher, M. (2016). Realismo capitalista: ¿No hay alternativa? Buenos Aires: Caja Negra.

Fisher, M. (2020). K-punk – Volumen 2. Escritos reunidos e inéditos (Música y política). Buenos Aires: Caja Negra.

ESPN. (2016). Atlético Nacional pidió que Chapecoense sea declarado campeón de la Copa Sudamericana. Recuperado de https://www.espn.com.co/futbol/copa-sudamericana/nota/_/id/2870888/atletico-nacional-pidio-que-chapecoense-sea-declarado-campeon-de-la-copa-sudamericana