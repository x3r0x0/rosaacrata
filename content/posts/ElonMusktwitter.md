---
title: "El multimillonario y los anarquistas."
date: 2022-11-26
draft: false
author: "x3r0x"
tags:  ["anarquismo", "twitter", "ELon Musk"]
images: ["https://cdn.crimethinc.com/assets/articles/2022/10/28/header.jpg"]
---

*Twitter desde sus orígenes como herramienta de protesta hasta la adquisición por parte de Elon Musk.* 

#### Te animamos a seguirnos en [**Mastodon**](https://hispagatos.space/@RosaAcrata)

Elon Musk ha tomado posesión de Twitter, afirmando que lo convertirá en "una plaza del pueblo digital común". ¿Qué clase de plaza del pueblo es propiedad 
de un solo plutócrata? La plaza de una ciudad empresarial -o de una monarquía-. ¿Qué significará esto para la gente corriente que depende de plataformas 
como Twitter para comunicarse y organizarse en la era digital?

# Resolver las tensiones dentro de la clase dirigente

Los conflictos que se produjeron en el seno de la clase capitalista durante la presidencia de Trump enfrentaron efectivamente a una coalición advenediza de
nacionalistas y capitalistas de viejo cuño (como el lobby del [petróleo](https://www.theguardian.com/us-news/2020/aug/09/big-oil-trump-campaign-donations-fossil-fuel-industry)) 
contra los partidarios del negocio neoliberal de siempre, ejemplificado por la gran mayoría de Silicon Valley. Si no fuera por estos conflictos intraclase, 
el esfuerzo de Trump por consolidar el control del gobierno estadounidense para su particular marca de autoritarismo nacionalista podría haber triunfado ya. 
Los movimientos de base encabezaron la resistencia a las políticas de Trump y el apoyo en la calle, pero Silicon Valley también tomó partido, culminando con 
la expulsión de Trump de su plataforma por parte de Twitter tras [el fallido intento de golpe de Estado](https://es.crimethinc.com/2022/01/06/january-6-first-as-farce-next-time-as-tragedy-what-if-we-knew-we-would-face-another-coup) del 6 de enero. Esto subrayó lo que ya estaba claro 
desde el [verano de 2020](https://es.crimethinc.com/2021/01/20/the-trump-years-the-road-from-january-20-2017-to-january-20-2021-a-chronology-of-resistance): Trump no había acumulado suficiente apoyo entre la clase capitalista para mantener su control del poder.

![BanTrumpTwitter](/images/3.jpg)
_Las decisiones de los anteriores administradores de Twitter durante la era Trump revelaron líneas de falla dentro de la clase capitalista._

¿Qué pasaría si Trump hubiera sido capaz de hacer causa común con una masa crítica de multimillonarios de Silicon Valley? ¿Habrían resultado las cosas de 
manera diferente? Esta es una pregunta importante, porque el conflicto a tres bandas entre nacionalistas, neoliberales y movimientos sociales participativos
no ha terminado.

Para decirlo en términos dialécticos vulgares:

Tesis: El esfuerzo de Trump por consolidar un nacionalismo autoritario
Antítesis: la oposición de los magnates neoliberales de Silicon Valley
Síntesis: Elon Musk compra Twitter

Entendido así, la adquisición de Twitter por parte de Musk no es sólo el capricho de un plutócrata individual: es también un paso hacia la resolución de 
algunas de las contradicciones dentro de la clase capitalista, para establecer mejor un frente unificado contra los trabajadores y todos los demás en el
extremo receptor de la violencia del sistema capitalista. Sean cuales sean los cambios que introduzca Musk, seguramente reflejarán sus intereses de clase 
como el hombre más rico del mundo.

De todos los gigantes de las redes sociales -y a pesar de la notoria presencia de Trump en la plataforma- los administradores de Twitter fueron posiblemente
menos complacientes con la agenda de Trump que los de Facebook o Youtube. Mientras que Mark Zuckerberg se reunió en repetidas ocasiones con Trump y sus 
partidarios de la extrema derecha, como Tucker Carlson, y Facebook e Instagram accedieron a las demandas de la extrema derecha de prohibir el acceso a sus 
plataformas a los anarquistas y antifascistas, Twitter prohibió el acceso a los fascistas al menos con la misma facilidad que a los anarquistas y otros 
activistas. En ese momento, especulamos que esto podría deberse a que Twitter todavía estaba bajo la dirección de algunos de los fundadores originales.

En este artículo, vamos a seguir el rastro de Twitter desde sus orígenes como herramienta de protesta para los activistas hasta la adquisición de Musk, 
esbozando una historia de la toma de posesión capitalista de Internet en un microcosmos.

### Innovación y cooptación

Con la compra de Twitter por parte de Musk, asistimos a la conclusión de un ciclo de innovación y cooptación en el ámbito de las comunicaciones. A finales 
del siglo XX, los modelos políticos y tecnológicos dominantes eran monolíticos y unidireccionales: televisión en red, partidos políticos de masas. 
En respuesta, los anarquistas y otros rebeldes experimentaron con medios de comunicación independientes y redes clandestinas, produciendo modelos 
innovadores horizontales y descentralizados como indymedia.org. Las empresas tecnológicas acabaron monetizando estos modelos como los medios participativos 
de la Web 2.0, como Facebook. Sin embargo, desde el cambio de siglo hasta la revuelta de 2020, los aspectos horizontales y participativos de Internet en 
general y de las redes sociales en particular siguieron dando poder a quienes buscaban una mayor autodeterminación, como las pintadas "Gracias Facebook" 
en Túnez tras las revueltas de la llamada "Primavera Árabe" de 2010-2011.

Sin embargo, en la última década, las empresas y los gobiernos han introducido cada vez más vigilancia y control en línea. La adquisición de Twitter por 
parte de Musk es la última etapa de una represión reaccionaria con consecuencias nefastas.

![GraffitiFacebook](/images/1.jpg)
_Febrero de 2011: pintadas en Túnez tras la caída del presidente Ben Ali. Fotografía de Habib Nassar._

Musk y sus colegas ven el capitalismo como una meritocracia en la que los competidores más astutos y trabajadores llegan inexorablemente a la cima. De ahí,
presumiblemente, su propio éxito.

Por supuesto, si Musk desea demostrar que su éxito no es sólo consecuencia del privilegio y la suerte -de la fortuna y la buena suerte- podría demostrarlo 
fácilmente regalando su riqueza, cortando sus lazos sociales, cambiando su nombre y repitiendo por segunda vez sus supuestas hazañas para hacerse rico. Si 
fuera capaz de escalar la pirámide por segunda vez sin el beneficio de haber crecido como blanco en la Sudáfrica de la época del apartheid (dejando a un 
lado la cuestión de las inversiones en esmeraldas de su padre por ahora), tendríamos que escuchar sus afirmaciones de que el mercado lo ha encumbrado 
gracias a sus cualidades personales, aunque eso seguiría sin demostrar que el capitalismo recompensa los esfuerzos que son más beneficiosos para la humanidad.

Según la narrativa de Silicon Valley, plataformas como Twitter son invenciones de emprendedores individuales, impulsadas por el capital financiero de 
astutos inversores.

Pero Twitter no surgió simplemente, completamente formada como Atenea, de la cabeza del cofundador de la empresa, Jack Dorsey. De hecho, fue un modesto 
perfeccionamiento de un modelo ya demostrado por TXTmob, el programa de mensajería de texto SMS desarrollado por el Instituto de Autonomía Aplicada para 
las protestas en las Convenciones Nacionales Demócrata y Republicana de 2004.

Blaine Cook y Evan Henshaw-Plath, desarrolladores anarquistas que trabajaron junto a Dorsey en su anterior empresa Odeo, ayudaron a perfeccionar TXTmob y 
más tarde se llevaron el modelo a las conversaciones con Dorsey que dieron lugar a Twitter.

Si la incesante urgencia de las redes sociales en general y de Twitter en particular puede resultar agotadora, es de esperar: la infraestructura de Twitter 
se diseñó originalmente para las comunicaciones callejeras durante las movilizaciones masivas de alto riesgo en las que la información debe difundirse 
inmediatamente, reducida a lo esencial. No es una coincidencia que, a pesar de sus deficiencias, la plataforma haya seguido siendo útil para los activistas 
callejeros y los periodistas de conflictos.

![twittergraffiti](/images/2.jpg)
_The good old days, when pro-Twitter graffiti appeared in Tahrir Square during the Egyptian Revolution._

La cuestión es que los modelos innovadores no surgen necesariamente del espíritu empresarial comercial de los grandes hombres de la historia y la economía.
Lo más frecuente es que surjan en el curso de los esfuerzos colectivos para resolver uno de los problemas creados por el orden capitalista. La resistencia 
es el motor de la historia. Después, los oportunistas como Musk utilizan la enorme ventaja económica que les otorga un mercado impulsado por los beneficios 
para comprar nuevas tecnologías y volverlas definitivamente contra los movimientos y los medios que las produjeron originalmente.

Podemos identificar dos fases en la apropiación capitalista del modelo TXTmob. En la primera fase, un marco que fue diseñado originalmente por voluntarios 
para el uso de manifestantes ordinarios se transformó en una empresa que cotiza en bolsa, más o menos al mismo tiempo que los espacios abiertos de la 
primera Internet estaban siendo colonizados por los sistemas de vigilancia con fines de lucro de la Web 2.0. En la segunda fase, esta empresa que cotiza 
en bolsa se ha transformado en el juguete privado de un único magnate con derechos, con consecuencias que aún están por ver.

Musk afirma que su objetivo es abrir la plataforma a un mayor número de discursos. En la práctica, no existe la "libertad de expresión" en su forma pura:
cada decisión que puede configurar las condiciones del diálogo tiene inevitablemente implicaciones sobre quién puede participar, quién puede ser escuchado
y qué puede decirse. Por mucho que digamos en su contra, los anteriores moderadores de contenidos de Twitter no impidieron que la plataforma sirviera a lo
s movimientos de base. Todavía tenemos que ver si Musk se dirigirá intencionadamente a los activistas y organizadores o si simplemente permitirá que los 
reaccionarios lo hagan en base al crowdsourcing, pero sería muy ingenuo tomarle la palabra de que su objetivo es hacer que Twitter sea más abierto.

### El multimillonario contra los anarquistas

Imagina que no crees que Elon Musk merezca tener más poder sobre lo que ocurre en Twitter que los aproximadamente 238 millones de personas que lo utilizan 
actualmente. A efectos de este experimento mental, imagina que crees que nadie merece tener un poder tan desproporcionado sobre el medio a través del cual 
los seres humanos se comunican entre sí. En otras palabras, imagina que eres anarquista.

¿Qué puedes hacer para que la gente pueda controlar las tecnologías que nos conectan? ¿Puedes establecer nuevas plataformas que respondan directamente a 
quienes las utilizan? Y lo que es más importante, ¿puedes popularizarlas, sacando a los usuarios de los corralitos cerrados de las redes sociales 
corporativas? ¿Puede reunir a la gente en otros foros, espacios que no puedan ser comprados y controlados por multimillonarios?

En efecto, la adquisición de Twitter por parte de Musk nos devuelve a los años 80, cuando los principales medios de comunicación estaban totalmente 
controlados por las grandes empresas. La diferencia es que las tecnologías actuales son participativas y no unidireccionales: en lugar de ver simplemente 
a locutores y celebridades, vemos representaciones de los demás, cuidadosamente seleccionadas por quienes dirigen las plataformas. En todo caso, esto hace
que las pretensiones de las redes sociales de representar los deseos de la sociedad en su conjunto sean más insidiosamente persuasivas de lo que podrían ser
los espectáculos de la televisión en red.

Es probable que el propio Twitter sea una causa perdida, pero no deberíamos ceder apresuradamente ningún territorio a través del cual podamos comunicarnos y
organizarnos contra nuestros opresores. En un mundo globalmente interconectado, nuestros adversarios en los gobiernos, las empresas y los movimientos 
reaccionarios seguirán aprovechando la tecnología digital para actuar con rapidez y coordinación. Nosotros no podemos permitirnos el lujo de no hacer lo 
mismo, aunque a la larga busquemos formas de conexión mucho más ricas que cualquier cosa que la tecnología digital pueda proporcionar.

Eres tú contra los multimillonarios. Ellos tienen a su disposición toda la riqueza y el poder del imperio más formidable de la historia del sistema solar.
Todo lo que tienes a tu favor es tu propio ingenio, la solidaridad de tus compañeros y la desesperación de millones de personas como tú. Los multimillonarios
triunfan concentrando el poder en sus propias manos a costa de todos los demás. Para que tú tengas éxito, debes demostrar formas en las que todos puedan ser
más poderosos. En esta contienda se enfrentan dos principios: por un lado, el engrandecimiento individual a costa de todos los seres vivos; por otro, el 
potencial del individuo para aumentar la autodeterminación de todos los seres humanos, de todas las criaturas vivas.

La buena noticia es que su relato sobre el origen de la innovación es una mentira. Los anarquistas tuvieron más que ver con los orígenes de Twitter que los
plutócratas como Musk. Podemos crear nuevas plataformas, nuevos puntos de partida para la conexión, nuevas estrategias para cambiar el mundo. Tenemos que
hacerlo.

https://twitter.com/rabble/status/1590853230208376832
https://twitter.com/blaine/status/1591189007798767616

- Traducido por x3r0x
- [Cuenta de x3r0x en Mastodon](https://hispagatos.space/@x3r0x)
- [Articulo original en ingles](https://es.crimethinc.com/2022/10/28/the-billionaire-and-the-anarchists-tracing-twitter-from-its-roots-as-a-protest-tool-to-elon-musks-acquisition)
