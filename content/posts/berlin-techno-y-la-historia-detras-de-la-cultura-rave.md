---
title: "Berlin, Techno y la Historia detrás de la Cultura Rave"
date: 2023-05-26
draft: false
author: "Catalina Dib"
tags:  ["rave", "lgbtiq+", "berlin"]
images: ["/images/BerlinTechno.png"]
---

#### Una revisión a los orígenes de la Cultura Rave de la mano del house en Chicago y su evolución hacia una cultura mundial que converge en la capital alemana.


![Berlintechno](/images/BerlinTechno.jpeg)

Corría la segunda mitad de la década de los ’80 y en varias ciudades del mundo se comenzaba a gestar un nuevo tipo de música basada en los sonidos electrónicos y en la repetición. Esta música, amplificada por los efectos del éxtasis, comenzó a crear un nuevo estilo de fiesta y de baile. Una danza individualista y rítmica, que va de la mano del beat. Y una forma de fiesta, el rave, liberal e inclusiva, donde se mezclan razas, orientaciones sexuales y cualquier otra forma de división, fundidas en la religión hacia la música y los sonidos. Va a ser Berlín, en su versión post muro, la ciudad que recogerá todas estas influencias internacionales, para posicionarse con los años, como el epicentro de la música electrónica y el estilo de vida rave.

![HouseChicago](/images/housechicago.png)


# EL HOUSE DE CHICAGO

La música house nace en Chicago a inicios de los '80 con pioneros como Ron Hardy y Frankie Knuckles (en la foto). A Knuckles se le conoce como el «Godfather of House». Era el DJ del club Warehouse (de donde viene el nombre house) donde mezclaba disco, funk, soul y hip-hop, con la nueva música creada con instrumentos electrónicos. Un amigo que estudiaba sonido, le comenzó a re-editar los discos, cortando la música a su medida; al ponerlos el público se volvía loco. En 1983 se compra su primer drum machine, generando los beats esenciales para el nuevo estilo de música.


#### The Warehouse, 1977 app.

![theWarehouse](/images/TheWarehouse.png)  ![theWarehouse](/images/warearehouse.png) 

En el Warehouse (en la foto) ya se comienza a gestar el mix cultural propio de la cultura rave. En un comienzo su público era más bien el de los segredados; negros, latinos y gays. Pero poco a poco comienza a formarse una escena más heterogénea, cuya principal preocupación era bailar y bailar. Hacia el '87, cuando los discos de Chicago estaban haciendo furor en el Reino Unido, la ciudad cambia su política de after-hours y la escena nunca vuelve a ser como antes.

![TechnoDetroit](/images/technoDetroit.png)  ![TechnoDetroit](/images/technoDetroit2.png) 

# EL TECHNO DE DETROIT

Entretanto, a mediados de la década, en la cercana Detroit, se comenzaba a crear un nuevo sonido derivado del house. El techno nace como una versión high-tech, industrial, más cruda y menos melódica que el house. Por su parte, la ciudad de Detroit tiene una curiosa historia que la relacionará con Berlin y su estética musical. En los años ’50 era una de las ciudades más prósperas de Estados Unidos, epicentro de la industria automovilística y sede de las principales fábricas; Ford, General Motors y Chrysler. Pero hacia los años ’60, con el traslado de la industria a países con mano de obra más barata, la Motor City se comenzó a despoblar. Ya en los ’80 era una ciudad abandonada, con un aire decadente y post-industrial, la misma que tendría Berlín, por otras razones, post muro.

![HeroesTechnoDetroit](/images/heoresdeDetroit) 

Los héroes del techno de Detroit eran parte de la comunidad negra. Derrick May, Juan Atkins y Kevin Saunderson eran tres compañeros de colegio que se escapaban a Chicago a escuchar a los nuevos DJs. Estos tres músicos se hicieron conocidos como de Belleville Three (en la foto) por su pueblo natal. Inspirados por Chicago comenzaron su propio club en el centro de Detroit; Music Institute.

![undergound](/images/underground.png) 

Por otro lado estaban los chicos de Underground Resistence; Jeff Mills, Mike Banks y Robert Hood, quienes tocaban un techno militante y político, totalmente anticomercial y asociado a la lucha de las igualdades raciales. Su sonido oscuro y futurista, será asociado posteriormente con el sonido berlinés y el club Tresor. Todos los precursores de Detroit citan al grupo alemán Kraftwerk como una de sus mayores influencias.

![acidhouse](/images/acidhouse.png) 

# ACID HOUSE EN U.K

En Inglaterra, hacia el año '87 comienza el furor por el acid house. Un estilo también creado en Chicago, pero que llegará al Reino Unido vía Ibiza. Esto sucede luego de que Paul Oakenfold y Dan Rampling se van de vacaciones a la isla. Allá, prueban el éxtasis por primera vez y pasan sus noches y mañanas en el club Amnesia bailando los beats del DJ argentino Alfredo. Al volver, expanden su experiencia, organizando raves y abriendo sus propios clubes.

![culturaAcid](/images/culturaAcid.png) 

La fiebre rave en Inglaterra se hace potente, la juventud necesitaba liberarse de las estrictas políticas conservadoras y materialistas del thatcherismo. La mayoría de las fiestas eran ilegales con venues en improbables lugares de los suburbios y avisadas a ultima hora. Era un movimiento psicodélico, con visuales tripeadas y colores neon. Su símbolo fue el smily, una cara feliz amarilla, que hacía un guiño a la alegría química del éxtasis. Fue una escena radical para Inglaterra que representaba el descenso de la violencia hooligans y un mix de gente único. Al verano del ’88 le llamaron el Second Summer of Love, por las reminiscencias con el movimiento hippie de San Francisco.

Pero las políticas reaccionarias se hicieron sentir con fuerza, las autoridades británicas entraron en pánico con una cultura que asociaban a las drogas y claramente a la libertad de expresión. Todo rave fue perseguido por la policía, en un despliegue que incluía hasta helicópteros. Hacia el año ’94 cuando la música electrónica estaba llegando al mainstream, se aprobó un acta que hizo imposible los raves. Ésta prohibió las reuniones al aire libre sin autorización, la música ruidosa y los «beats repetitivos».

![freedomtoparty](/images/freedomtoparty.png) 

# BERLIN Y LA ALEMANIA UNIFICADA

Mientras tanto, en Berlín caía el Muro. Berlín Oeste ya era un lugar para la cultura alternativa en Alemania, pero con la caída del muro se abrían enormes posibilidades. En Berlín del Este un sistema completo colapsaba. La GDR dejaba de existir generando un vacío de poder que las autoridades de la Alemania unificada se demoraron un par de años en llenar. Más de un tercio de los edificios en Berlin del Este quedaron abandonados. Era una ciudad fantasma con un aura de postguerra. 1990 fue el «verano okupa», un verano que necesitaba su propia banda sonora.

![berlin](/images/berlin.png) 

Y esta banda sonora va a surgir justamente de la unión. West Berlin, aunque era más rockera, ya tenía su propio club de acid house, UFO, donde tocaban DJ Westban y Dr. Motte. Por su parte, los chicos del Este buscaban un sonido mucho más duro, sin vocales ni piano. Con espiritu okupa, comenzaba la aventura de entrar a lugares en desuso, instalar un equipo de sonido, correr la voz y empezar el rave eterno.

![berlin2](/images/berlin2.png) 

Esta primera época estuvo determinada por una actitud nómade y experimental. No habían planes inmobiliarios, ni objetivos monetarios, lo único que se buscaba era bailar y bailar ante este nuevo sonido y en este ambiente de libertad. Solo imaginemos la sensación de bailar en un lugar donde hasta hace unos meses te podían disparar.

Desde el comienzo la arquitectura y el uso creativo de los espacios fueron fundamenales en la personalidad de los clubes, incluso dándole forma a su identidad musical. Un emblema de la época era Der Bunker. Este club ocupó un edificio construido por los nazis el ’43 como refugio anti-aéreo para los pasajeros del tren suburbano. Con capacidad para 3000 personas y paredes de dos metros de ancho. Hoy alberga la colección de arte de la familia Boros, abierta al público con previa cita. El club E-Werk se ubicaba en una planta eléctrica olvidada cerca de Checkpoint Charlie. Tocaban Westban, Paul van Dyck y Woody.

![berlin3](/images/berlin3.png) 

En esta tríada icónica también estaba Tresor. De los mismos creadores de UFO, este club que existe hasta el día de hoy, fue fundamental en cimentar lo que sería el sonido berlinés y el espíritu de la ciudad. Su ubicación, hasta el 2005, fue la bóveda subterránea de una tienda por departamento en Leipziger Platz, que estaba abandonada desde la IIGM. En los inicios se escuchaba en el club una mezcla de sonidos internacionales, entre gabba holandés, hardcore belga, acid house de U.K y techno de Detroit (UR y el sello Plus8). Pero va a ser finalmente este último estilo el que le dará su sello generando la famosa conexión Berlín-Detroit.

![berlin4](/images/berlin4.png) 

# CONEXIÓN BERLIN-DETROIT: HARD WAX Y TRESOR

Berlín fue de alguna manera el heredero del sonido de Detroit, ese sonido oscuro e industrial que resonaba a la perfección con la capital alemana. Y uno de los grandes provocadores de esta conexión fue Mark Ernestus (en la foto), creador de la tienda de discos Hard Wax. Hard Wax abrió un mes después de la caída del muro y aún hoy es un lugar de peregrinación para los músicos por su colección única y perfectamente estructurada. Ernestus, en su afán por encontrar los nuevos sonidos, comienza a comprar los discos que estaban lanzando los sellos underground de Detroit. Finalmente viaja a conocer a los músicos y los termina invitando a Berlín.

![berlin5](/images/berlin6.png) 

Los primeros en llegar fueron los de Underground Resistance. Comienzan a tocar en Tresor (que se vuelve muy purista del sonido de Detroit) y a grabar discos con sellos berlineses en el estudio de Thomas Fehlman. Para ellos, altamente políticos, los alemanes también se querían liberar de un pasado de opresión y división. Para Robert Hood «la musica experimental, futurista, era como una nave espacial con la que podías huir… la esperanza de un futuro mejor»

![berlinDetroit](/images/berlinDetroit.png)

# EL HOUSE ENTRA EN EL MAINSTREAM

A mediados de los '90, la música electrónica asciende al mainstream. Love Parade, el famoso festival de Berlín, comenzó el año ’89 con 150 personas y hacia el ’96 ya asistían casi un millón. Los Djs comenzaron a ganar fortunas y a ser tratados como estrellas de rock con managers y jets privados. Aparecieron los super clubs liderados por los de Ibiza como Amnesia y Pacha. La parafernalia era constante, los colores neones se volvían vulgares y las plataformas enormes no parecían tan aptas para bailar. En general la escena perdió su edge. Lo que había comenzado como algo espontáneo, de celebración a la música y a la libertad, se comenzó a estructurar bajo la mano del business; el rave comenzó a ser lucrativo.


![berlin7](/images/berlin7.png)

Alemania llevó la delantera en esta masificación de la cultura del rave. Tanto así, que muchos estudiosos, le asignan el valor de ser la primera área en donde la cultura alemana se reunifica. También es muy interesante pensar que fue la primera vez que Alemania se convierte en el centro de la cultura pop y de un movimiento juvenil, ambos campos históricamente dominados por el mundo anglo-sajón. Y quizás una de las grandes razones es que en la música electrónica no se necesitan los idiomas.


# BERLÍN Y EL RAVE COMO ESTILO DE VIDA

En Berlín realmente el rave es un estilo de vida, es una energía que se respira constantemente en la ciudad. El beat es parte del día a día y lo puedes escuchar a cualquier hora en cualquier lugar. Incluso la moda en Berlín esta directamente influenciada por el rave; nadie usa taco alto, la mochila reina y el negro democratizador es el uniforme. Como dijo Felix Denk, de forma muy acertada (periodista especializado en música que escribió el libro «Der Klang del Familien») a la T Magazine del New York Times, «Techno is like our folk music here». 

Además Berlín es una ciudad de espíritu liberal y ha pesar de lo que muchos piensan, siempre lo ha sido. Previo al nazismo (que nace en Bavaria, en el sur de Alemania) se vivía el esplendor de las artes y las ciencias en la República de Weimar. Y luego, después de años de represión, la manguera se destapó con fuerza, convirtiendo a la ciudad en un centro de la actividad creativa europea.
