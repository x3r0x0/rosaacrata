---
title: "Los anarquistas:HBO"
date: 2022-08-06
draft: false
tags: ["Falso Anarquismo","FuckAncaps"]
images: ["/images/Theanarchists.jpg"]

---
### HBO y su documental de mierda
--- 

![Documental de mierda](/images/Theanarchists.jpg)

Si has pasado cualquier cantidad de tiempo en las tierras desoladas conocidas como Internet en 2022 este verano,
probablemente has oído hablar de la reciente serie documental de HBO titulada "El Anarquista". 
Esta patética excusa para una documetal-serie sigue la historia de un blanco canadiense adinerado que inició una conferencia en México llamada 
"Anarchapulco", con la esperanza de promover el anarcocapitalismo. La serie sigue a una serie de occidentales ricos que han decidido que México 
es la tierra de la libertad que les salvará de las asfixiantes burocracias de la vida moderna.
Después de ver el primer episodio estamos aquí para decir que el "anarcocapitalismo" no es anarquismo.
De hecho, es literalmente lo contrario del anarquismo. Mientras que los anarquistas buscan abolir todas las formas de dominación y jerarquía, 
estos libertarios lo fomentan mientras se disfrazan de "antiestatistas rebeldes" que apoyan la tiranía privada, la subyugación a través de la 
propiedad privada de los recursos, y el uso de polos de colores pop como si todavía fuera 2004.
Los "anarquistas" de este programa no tienen ninguna praxis, excepto la de hacer círculos sobre su odio compartido por el pago de impuestos, 
la compra de criptodivisas y la simulación de ann raynd. Los anarcocapitalistas creen que la sociedad debe ser dirigida por un puñado de empresas 
que buscan beneficios sin restricciones y piensan que mientras todos los gobiernos son deshonestos y egoístas se les debe permitir ser tan egoístas 
y deshonestos como quieran.

- [Submedia Presents:The Anarchists] (https://kolektiva.media/w/bEpxLm6E8Qb7h8PgpFujsC) 
