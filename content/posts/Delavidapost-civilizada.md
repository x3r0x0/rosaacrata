---
title: "De la vida post-civilizada y de las ciudades que no son ciudades - visiones de un futuro anarquista."
date: 2022-10-15
draft: false
author: "Margaret Killjoy"
tags: ["AntiCiv"]
images: ["/images/EndCiv.jpg"]
---

## Coge lo que necesites y tira el resto al abono, o: Una introducción al pensamiento postcivilizado

Lo de la civilización fue emocionante, ¿no? Quiero decir, definitivamente valía la pena intentarlo. La civilización nos ha aportado mucho: 
telescopios, sillas de ruedas, Wikipedia. 
Pero en el proceso ha llevado a la naturaleza al borde del abismo. La ciencia, la agricultura y la división del trabajo nos han ayudado a 
desarrollar nuestra cultura y comunicación, pero han sido aún más útiles para los genocidios y los ecocidios. Por tanto, parece que ha 
llegado el momento de abandonar el ambicioso pero fallido experimento de la civilización y buscar algo nuevo.

## Primer requisito: odiar la civilización

La civilización no es sostenible. Parece imposible salvarla. Y lo que es más importante, tampoco sería deseable. Cuando se habla de "civilización", 
se habla de las formas organizativas y las expresiones culturales del mundo moderno; se habla de las normas jurídicas y sociales que dictan el 
comportamiento "correcto"; se habla de las pretensiones centralistas y totalitarias del imperio político-económico.

La civilización está destruyendo todas las formas de vida en la tierra. No es sorprendente que no sea sostenible: las economías y sociedades 
orientadas al crecimiento nunca lo son. Las posibilidades de mantener la civilización sin un consumo excesivo de sus recursos son casi nulas. 
Y aunque las hubiera, no querríamos aprovecharnos de ellas. Nuestra libertad se vería entonces más restringida.

La civilización se ha definido de muchas maneras, pero ninguna definición es atractiva. Mi diccionario dice que la civilización es "la forma 
más avanzada de desarrollo y organización social". Aparte de ser una definición bastante inútil, apunta a una presuposición implícita de la 
civilización: "Nosotros somos avanzados, vosotros sois primitivos. La historia y el desarrollo son lineales, el progreso es unidimensional, 
y cualquier desviación de eso es regresiva".

Otra definición estándar de "civilización" puede encontrarse en Wikipedia, el crisol del consenso social. Aquí se describe la civilización como
"una sociedad compleja caracterizada por la agricultura y la urbanidad. ... En comparación con las sociedades menos complejas, las personas se 
dedican a tareas laborales específicas y existen formas de organización jerárquicas". Esta definición apunta también a un problema fundamental 
de la civilización: ¿"formas de organización jerárquicas"? ¿Qué es esto? ¿Por qué la gente aguanta estas cosas?

Derrick Jensen, un crítico de la civilización (aunque no de la poscivilización) ofrece la siguiente definición de civilización: 
"Una cultura -es decir, un complejo de narrativas, instituciones y mercancías- que conduce a la urbanización y se reproduce en ella. (Civil viene 
del latín civis o civitas, que significa ciudad-estado)". Esta definición plantea la siguiente cuestión: ¿Qué es una ciudad? Derrick lo define como 
un lugar en el que "la gente vive de forma más o menos permanente con una densidad lo suficientemente alta como para que la importación permanente 
de alimentos y otros recursos vitales sea una condición de supervivencia."

Este es posiblemente el punto crucial. Si un lugar necesita recursos de otro, eso no es un problema en sí mismo, siempre que haya algo que intercambiar. 
¿Pero qué pasa si hay una sequía y los campesinos ya no tienen excedentes de alimentos para comerciar? Entonces llega la guerra. Genial.

Odiemos la civilización.
## Segunda condición: No ser primitivistas
No es posible ni deseable volver a formas de existencia precivilizadas. La mayor parte de la teoría crítica con la civilización ha sido formulada por los 
primitivistas. Han hecho un trabajo enormemente importante. Creen, a grandes rasgos, que sería mejor para la humanidad volver a una vida precivilizada. 
No compartimos esta conclusión.

Los primitivistas se oponen a toda forma de tecnología. Sólo nos oponemos al mal uso de la tecnología. Pues bien, si somos sinceros, en la práctica esto 
significa que también estamos en contra de casi todas las formas de tecnología civilizatoria. Pero aun así, creemos que la crítica primitivista está 
tirando el bebé con el agua del baño. Es cierto que la mayoría de las formas de tecnología sirven para fines malignos (como la guerra o el ecocidio), 
pero eso no hace que la tecnología en sí misma, o "la aplicación del conocimiento científico para el beneficio práctico", sea maligna. Sólo significa 
que tenemos que desarrollar una nueva forma de tratar con las máquinas, las herramientas y con la propia ciencia. Tenemos que centrarnos en lo que 
es realmente útil y sostenible, más que en el valor económico o militar.

Los primitivistas están en contra de toda forma de agricultura. Sólo estamos en contra de los monocultivos, que son centralizadores, destruyen la 
independencia regional, imponen la globalización en el mundo y conducen a prácticas terribles como la tala y la quema. Tampoco estamos de acuerdo 
con la idea de enviar a seis mil millones de personas a los bosques a cazar y recolectar. Eso no es una solución al problema alimentario. 
Sencillamente, los poscivilizados estamos a favor de la permacultura: de una agricultura cuya sostenibilidad regional esté asegurada desde el principio.

Los primitivistas han hecho un gran trabajo al presentar los problemas de la vida civilizada. Se merecen un reconocimiento por ello. Sin embargo, en 
general, su crítica no es muy diferenciada.

Lo que resulta especialmente problemático es que la estructura social que tienen en mente, a saber, la cultura tribal, tiene tendencias socialmente 
conservadoras (aunque queremos dejar claro que las críticas habituales a la cultura tribal se basan en supuestos falsos y eurocéntricos): el papel 
de las leyes estrictas ha sido asumido por las "costumbres" rígidas en muchas tribus, y cada generación ha tenido que seguir más o menos los pasos 
de la anterior.

Miles de millones de personas no pueden volver a un modo de vida precivilizado. Y, francamente, la mayoría de nosotros no queremos hacerlo. Así que 
no podemos estar de acuerdo con un rechazo total de los productos de la civilización. Tenemos que mirar hacia adelante, no hacia atrás.

No seamos primitivistas.

## Tercera condición: Seamos poscivilizados

Lo que necesitamos es una cultura post-civilizada. Y la construcción de uno puede comenzar en el aquí y ahora, en medio de la fase final de la 
civilización.

El mundo está lleno de falsas dicotomías. El hecho de que los músicos puedan vivir de la música o no, no determina su valor; la diferencia entre 
"profesionales" y "aficionados" no es válida. Para los postcivilizados, la diferencia entre habilidades "generales" y "específicas" tampoco importa, 
hay que combinar ambas. Necesitamos personas que puedan moler vidrio y producir vasos, pero eso no significa que esas personas no deban también 
cocinar o desherbar.

Uno de los mayores problemas de la civilización es intentar construir una cultura global, o un modelo de cómo hacer todo tipo de tareas -desde la 
administración hasta la arquitectura, pasando por la agricultura o la música- de forma adecuada. Eso no tiene sentido. Si se construyen casas con 
tejados planos en un clima frío, el tejado se derrumbará en cuanto se acumule suficiente nieve en él. Si se talan árboles en una ladera como se 
haría en una llanura, se producirá un desprendimiento.

Si queremos pasar a un modo de vida postcivilizado (con o sin colapso industrial), primero debemos estudiarnos a nosotros mismos, a nuestra 
comunidad y a nuestra región, y luego decidir qué modo de vida es el más adecuado. Esto también significa que hoy podemos utilizar recursos 
que son producto de la civilización y que pueden no existir dos generaciones después del colapso. Para los habitantes del Primer Mundo, el
más rico de estos recursos son los residuos.

Gran parte de los alimentos que se tiran siguen siendo comestibles. Y la comida podrida se puede compostar. Este abono puede colocarse sobre 
los suelos envenenados de las ciudades para hacer posibles los jardines. El papel que sólo se ha utilizado parcialmente (o no se ha utilizado 
en absoluto) puede convertirse en bloc de notas. Podemos batir el papel usado hasta convertirlo en pulpa en una batidora y prensarlo en papel 
nuevo con un gato hidráulico. Los animales atropellados pueden ser desollados y comidos. Las placas de circuitos y los motores de los juguetes 
eléctricos pueden utilizarse para todo tipo de cosas. El aceite vegetal usado puede recuperarse de los filtros de grasa para alimentar coches
o generadores.

Los críticos de nuestra actitud suelen decir que esto no puede funcionar siempre. Tienen toda la razón, pero pasan por alto uno de nuestros 
principios fundamentales: nos adaptamos a la situación en la que nos encontramos. Por supuesto, lo que funciona aquí y ahora no puede funcionar 
siempre en todas partes.

En la lógica de la civilización, hay un desarrollo gradual: la cultura se extiende de los civilizados a los salvajes y de las ciudades al campo. 
Nosotros lo vemos de otra manera.

Seamos postcivilizados.

## Si dependiera de nosotros...

¿Cómo es una ciudad que no es una ciudad? El concepto de la ciudad como una entidad separada con fronteras específicas, un gobierno centralizado 
y una importación regular de bienes debe desaparecer. Pero, en cambio, no todos montaremos nuestras tiendas en el campo. ¡Claro que no!

La ciudad postcivilizada (o la "no-ciudad" o el "espacio urbano" - no es tan fácil con la terminología) puede parecer una ciudad contemporánea que 
ha perdido su gobierno. Sería un conjunto de pequeños grupos, todos ellos conservando su identidad pero trabajando juntos por el bien común.

Los poscivilizados queremos demostrar que la descentralización de nuestra cultura, economía y política es posible y deseable. Cada grupo más pequeño
tomaría sus propias decisiones, mantendría su independencia y resolvería los problemas de la forma que más le convenga. Algunos pueden utilizar 
tecnología avanzada para satisfacer sus necesidades y deseos, otros pueden vivir con toda sencillez. Pero las fronteras entre los grupos serán 
muy permeables y los individuos, las familias y los círculos de amigos irán de un lado a otro. La vida cotidiana se asemejaría probablemente a 
la actual, pero sin la jerarquía civilizatoria ni la centralización.

¿Se enfrentarán estos grupos entre sí? Probablemente. Ningún sistema es perfecto, y es mejor admitirlo abiertamente que negarlo. No estamos 
pintando una utopía en la pared. Al mismo tiempo, hay ejemplos históricos de estructuras políticas que permiten que grupos con intereses 
diferentes convivan y trabajen juntos de forma pacífica. Por ejemplo, podemos aprender muchas lecciones de la historia del sindicalismo.

El sindicalismo supera la oposición del capitalismo y el socialismo de Estado. Se basa en el hecho de que una federación de sindicatos 
colectivizados promueve la ayuda mutua entre sus miembros. El ejemplo histórico más exitoso de esto es la Guerra Civil española.

La ayuda mutua es lo contrario de la competencia. Wikipedia lo describe como "el concepto económico de intercambio voluntario de recursos 
y servicios para el bien de todos". Uno de los primeros anarquistas, Peter Kropotkin, fue también uno de los primeros teóricos de la evolución. 
Se opuso a la afirmación de Darwin de que el estado de naturaleza era una guerra de todos contra todos. Por el contrario, Kropotkin creía que 
la cooperación entre especies desempeñaba un papel al menos tan importante en la historia de la evolución como la competencia. Hoy en día, la 
ciencia moderna ha empezado por fin a creer en esta tesis.

Sin embargo, no podemos reducirnos al sindicalismo. El sindicalismo es una gran idea, pero no se trata de sindicatos ni de industrialización. 
No suscribimos los principios del anarquismo histórico como tampoco los del feminismo de segunda ola o de la civilización. Se trata de grupos 
dinámicos de personas que se reúnen para dar forma juntos a la no ciudad.

Hablamos de steampunks que añaden lentes de Fresnel a sus destilerías solares; hablamos de fanáticos de la bicicleta que convierten los recados 
en carreras y construyen bicicletas con tubos viejos; hablamos de adolescentes seminómadas que pastorean cabras en suburbios abandonados; hablamos 
de ermitaños que cultivan patatas en neumáticos apilados y graban piano clásico en rodillos de cera. Además, alguien conectará su Super Nintendo 
a un panel solar y gente de todo tipo vendrá a jugar a Street Fighter o a ver jugar a otros. Todos cultivaremos la mayor parte de nuestros alimentos 
y nos desharemos de nuestros residuos y lavaremos nuestra propia vajilla.

## El colapso

Si fuera por nosotros, nos despediríamos de la civilización lo más pacíficamente posible. Nos organizaríamos en grupos de base y utilizaríamos 
argumentos convincentes para atraer a nuestro lado a los gobernantes que aún tienen algo parecido a una conciencia ética, mientras que dejaríamos 
sin poder a los incorregibles en cuanto un número suficiente de personas se negara a seguir el orden civilizatorio de intercambio.

Pero, para ser sinceros, este escenario no es muy probable. Nuestra sociedad está en curso de colisión con la historia. Supongo que la única pregunta 
relevante es qué se derrumbará primero: ¿la civilización industrial o la capacidad de la Tierra para sostener la vida humana? De estas dos posibilidades,
la primera es, con mucho, la más atractiva. Y debemos actuar en consecuencia.

El colapso de la civilización industrial será terrible. Ninguno de nosotros, ni siquiera los que secretamente esperan este colapso, lo disfrutará. 
Pero Hollywood miente: los tiempos de crisis no sacan lo peor, sino lo mejor de las personas. Nada une tanto a los habitantes de un barrio como un apagón.
Nunca se comparte tanta comida como cuando hay escasez de suministros. (¿De verdad crees que todos nos sentaríamos sobre nuestras provisiones, 
nos dispararíamos unos a otros y quemaríamos las casas de nuestros vecinos en una situación así? Esto no es necesariamente así. ¿Qué crees que somos? 
Civilizado).

Si el sistema económico no se derrumba y no encontramos una forma de hacer fusión fría (y también una forma de volver a llenar los océanos de peces), 
nos espera algo mucho, mucho peor: un colapso ecológico que no dejará piedra sobre piedra. Si algunos de nosotros sobrevivimos, nada volverá a ser igual.

Para que la civilización no nos destruya, debemos destruirla, y cuanto antes.

## Mientras tanto...

Ya no queremos ser civilizados. Ha llegado el momento de hacer algo diferente. Queremos superar las jerarquías y los sistemas económicos insanos, 
al igual que el colonialismo y los estados nacionales. Pero "salir" no es realmente posible. La civilización nunca -ni una sola vez en su historia-
ha dejado espacio para los que no son civilizados. El miedo a que la gente pueda probar la posibilidad de una vida mejor es tan grande que la civilización
simplemente no puede permitir otras formas de vivir. Esa es su característica definitoria.

E incluso si algunos de nosotros pudiéramos "excluirnos", ¿eso detendría la destrucción civilizatoria de la Tierra?

Pero seamos optimistas por un momento y no pensemos demasiado en el futuro. Independientemente de que la Tierra se destruya o no y de que la civilización
se derrumbe o no, ¿qué nos queda por hacer en el aquí y ahora?

No quiero llamar aquí a una lucha épica para salvar la tierra, destruir la civilización o colapsar esto o aquello. Las decisiones éticas de este tipo
deben ser tomadas por todos y para todos.

Sin embargo, me gustaría abogar por un estilo de vida post-civilizado. La vida post-civilizada no es tan difícil. Cierra los ojos e imagina quién 
serías sin limitaciones sociales. ¿Qué harías si sólo dependieras de ti mismo, de tus amigos y de los recursos que encuentras a tu alrededor? 
¿Qué te pondrías? ¿Qué comerías? Quizá las cuestiones más importantes sean más sutiles: ¿Cómo tratarías a tus amigos? ¿Cómo te gustaría que te trataran?

En el aquí y ahora podemos aprender técnicas de supervivencia: el desollado, el curtido y el tendido de cables, el tiro con arco y la producción de 
pólvora, la fitoterapia y la acupuntura, pero también el uso y la producción de antibióticos, la cirugía y la odontología. Seguimos los principios 
de la permacultura, descubriendo nuestro interior salvaje y utilizando cualquier cosa útil que podamos encontrar en las ciudades, los suburbios y 
el campo. Aprendemos a existir de forma sostenible en un mundo moribundo. Convertimos el césped en jardines y las carreteras en carriles bici.

Resolvemos juntos los problemas de nuestra subcultura; aprendemos a lidiar con las agresiones físicas y sexuales sin llamar a la policía; aprendemos 
lo que es un trauma (casi siempre por las malas) y cómo afrontarlo; criamos gallinas y patos; comemos dientes de león y eneas.

Vivimos como podemos, como si la civilización fuera un mal recuerdo ya superado. Eso -más que cualquier texto- será nuestra propaganda más eficaz. 
Porque es posible vivir así. Y, sí, es mejor vivir así. Una comida significa mucho más cuando uno mismo ha cultivado o recolectado los alimentos, 
y los amigos están mucho más cerca cuando todos son tratados como iguales. Somos salvajes con cola. Cogemos lo que necesitamos y tiramos el resto 
al compost.

## Recolección contra la civilización

Cuando la gente civilizada tiene hambre, elige una receta del libro de cocina y va a la tienda a comprar los ingredientes. Cuando los recolectores 
tienen hambre, se hacen una idea de los alimentos disponibles y preparan una comida basada en ellos. Los recolectores están siempre al acecho de la
comida: en los jardines, en los cubos de basura, en los mercados donde se regala comida por la noche o en los espacios verdes donde crecen las plantas
silvestres.

Como no es difícil de adivinar, los postcivilizados nos ceñimos a los recolectores. Y esto se aplica no sólo a nuestra comida, sino también al arte, 
la ciencia o la pedagogía. Hay muchas razones para ello: la estética es, sin duda, una de ellas.

En el pensamiento civilizatorio, la productividad cumple su propia finalidad: las empresas de automóviles fabrican coches porque eso es lo que hacen. 
Nunca se plantea la cuestión de si no hay ya suficientes coches. (La respuesta, por supuesto, sería un rotundo sí. Incluso si queremos seguir viviendo 
con el transporte individual, hay suficientes coches para hacerlo. Sólo hay que mantenerlos adecuadamente, repararlos y, en algunos casos, reconstruirlos).
Se están talando bosques para construir nuevas casas mientras que innumerables edificios permanecen vacíos.

Este comportamiento no se corresponde con el comportamiento inteligente e ingenioso del animal (humano) que hemos evolucionado. Es el resultado de la 
cultura civilizada.

Las personas civilizadas eligen su ideología como eligen su teléfono: miran diferentes versiones y luego eligen una de ellas. Los coleccionistas 
desmontan las ideologías, se quedan con las partes interesantes y las vuelven a montar para crear sus propias visiones del mundo.

Coleccionar es muy parecido a piratear, y viceversa.

Ahora puedes pensar que esto puede estar bien para una "pequeña minoría" pero nunca puede funcionar para la sociedad en su conjunto. "¿Quién va a 
cultivar alimentos? ¿Quién construiría mesas?" También puedes gritar estas preguntas con rabia, gesticulando salvajemente. Esto hace que el debate 
sea especialmente interesante.

Sobre todo porque tendrías un punto. La mayoría de nosotros vivimos en lugares donde la densidad de población es demasiado alta para depender 
únicamente de la recolección. Pero no propagamos una vida clásica de cazador-recolector. Podemos cultivar alimentos. Pero no en forma de monocultivos 
que producen maíz para la exportación. Y cuando construimos mesas, utilizamos el material del que disponemos, y sólo construimos las mesas que
realmente necesitamos.

La "pureza" no es nuestro objetivo en ningún caso. En absoluto.

## Recolección contra el capitalismo

Lo fundamental es que apliquemos los recursos que ya existen antes de buscar otros nuevos.

Para ello, por supuesto, hay que transformar la sociedad. ¿Cómo lo hacemos? El concepto de revolución es siempre una posibilidad, pero no tiene muchos 
éxitos que mostrar. ¿El colapso? Sí, es probable que la civilización se cuide a sí misma, al menos globalmente. Pero, ¿quién quiere esperar a que se 
quemen los continentes y se vacíen los océanos? ¿Quién quiere perecer así?

El enfoque de la poscivilización consiste en empezar a vivir la poscivilización aquí y ahora, independientemente de un posible "apocalipsis revolucionario"
(rev-ocalypse). ¿Pero cómo lo hacemos?

Nada de lo que escribo en estos textos debe entenderse como instrucciones. Sólo estoy formulando algunas ideas.

Una de ellas es sustituir la economía de mercado capitalista, y hacerlo inmediatamente. Los movimientos cooperativos y el sindicalismo de los siglos XIX 
y XX iban por el buen camino: las cooperativas dejaban de lado a los especuladores comerciales y se dirigían directamente a los consumidores, ahorrando 
dinero a todos. Y los sindicalistas tomaron el control de la industria despidiendo a sus jefes y haciendo que todos trabajaran como iguales. Pero el
dinero y la industria no son realmente lo que nos interesa; en cualquier caso, no pueden desempeñar el papel que tienen hoy en el futuro.

Dado que la mayoría de los elementos materiales que necesitamos ya están fabricados, pueden distribuirse gratuitamente. Las tiendas gratuitas son tiendas
de segunda mano gestionadas por voluntarios en las que no hay que pagar nada.

Sin embargo, a menudo estas tiendas están aisladas y no pueden redistribuir la enorme masa de bienes que se tiran cada día en el mundo civilizado. Por 
lo tanto, sugiero lo siguiente para cada ciudad:

- Alquilar o comprar un almacén. Almacena allí los artículos donados o encontrados.

- Alquilar, comprar u ocupar tiendas en todos los barrios de la ciudad. Distribuir la mercancía.

Cuanto más se puedan satisfacer las necesidades fuera de la lógica de la economía de mercado, más independientes serán las personas de la economía de 
mercado. Cuanto menos compre la gente, más sufrirá el sistema capitalista y más fuertes serán los contextos económicos alternativos. En algún momento,
el viejo orden caducará y la economía del regalo crecerá e irá más allá de las tiendas gratuitas; incluirá alimentos, servicios y eventos artísticos.

A nivel local, hay dos obstáculos principales que superar: Control de alquileres y aislamiento.

Una red de tiendas (con un almacén central) puede mitigar el riesgo de aislamiento. Muchas personas se sienten intimidadas por la naturaleza subcultural 
de los círculos radicales. A veces la respuesta es suavizar el contenido político o parecer "normal". Este enfoque puede describirse como el del mínimo
común denominador. Explica, entre otras cosas, por qué una democracia basada en el voto mayoritario es tan aburrida.

Una diversidad de culturas es mucho mejor que una cultura unidimensional y homogénea. Esto también se aplica a los círculos radicales. El almacén central
puede funcionar como base común para todos los grupos o culturas, mientras que las tiendas pueden ser tan individuales -y subculturales- como quieran. 
Lo único importante es que todos participen en el proyecto.

La cuestión del alquiler es más complicada. Las tiendas podrían funcionar sobre la base de donaciones mensuales voluntarias. Estos no darían a los 
donantes ninguna ventaja específica (como poder elegir primero los nuevos bienes), sino que les animarían a dar parte de sus ingresos mensuales al 
proyecto comunitario. Es importante mantener los costes lo más bajos posible. Lo ideal sería no necesitar empleados remunerados (¡todos deberían poder 
vivir de la economía del regalo!), el transporte se haría con bicicletas convenientemente equipadas, y las tiendas individuales contarían con personal 
si fuera posible.

Todo esto no es fácil, pero es factible. Sobre todo porque en un momento en el que suenan las campanas de muerte del orden económico imperante, la 
necesidad de formas alternativas de economía es mayor que nunca, y con ella la oportunidad de establecerlas.

## La vida poscivilizada

¿Así que has decidido dejar atrás la civilización? Enhorabuena. Hay muchas razones para hacerlo.

Quizá hayas visto películas postapocalípticas o hayas leído libros y cómics postapocalípticos, y ahora quieras un mundo en el que puedas vivir como 
quieras. Tal vez haya comprendido los costes del sistema imperante, y que sus raíces históricas se encuentran en que la gente empezó a encerrar los 
alimentos y a darlos sólo a cambio de trabajo. Tal vez esté tan enfadado por la destrucción causada por el sistema dominante que quiera verlo eliminado, 
o al menos ignorado. Tal vez te guste cultivar y recolectar tus propios alimentos, pero sigues queriendo vivir en las ciudades.

Sea cual sea el motivo, nos complace darle la bienvenida a las filas de los poscivilizados.

Algunas sugerencias:

- Estudia cuidadosamente tu entorno. Investiga qué recursos están disponibles y planifica tu vida en consecuencia.

- Recicla y reutiliza todo.

- Evita el uso del dinero siempre que un problema pueda resolverse de alguna manera sin dinero.

- Encuentra gente que piense como tú. Únase a ellos. A continuación, se conecta en red con otros grupos.

- Especialícese en una o dos áreas y adquiera conocimientos básicos en el mayor número posible de otras.

Este consejo se aplica principalmente a los Estados Unidos y otros países "desarrollados". Sin embargo, muchos de los consejos son aplicables en otras 
partes del mundo, aunque los postcivilizados siempre deben tener en cuenta las condiciones locales y regionales.

## Alimentos

En el mundo "desarrollado", esto es lo más fácil. Encontrar comida puede llevar mucho tiempo y energía, pero es más fácil de gestionar que las cuestiones
de vivienda o atención sanitaria.

La comida está en todas partes. Incluso en las ciudades crecen plantas comestibles. En la última ciudad en la que viví, comíamos dientes de león (se cogen
las hojas jóvenes y se cuecen al vapor mientras se cambia el agua varias veces), trébol (directamente del suelo), bellotas (el tanino desaparece dejando 
las bellotas en un arroyo durante unos días o hirviéndolas mientras se cambia el agua - luego se pueden convertir en harina) y diversos frutos secos, por 
ejemplo, castañas. Se puede hacer mermelada con las naranjas ornamentales y harina con las flores del árbol de la vaina dulce. Los árboles frutales son 
abundantes y los propietarios privados no suelen tener problema en que otros se lleven algunos (o simplemente no se dan cuenta). Si eres carne, siempre 
hay animales que son atropellados. Fuera de la ciudad, por supuesto, estos son más grandes y más sabrosos. Sin embargo, hay que tener cuidado con los 
animales muertos en la carretera. La carne debe ser fresca y estar bien preparada. ¡Infórmate lo mejor posible! Sin embargo, mis textos no serán de 
mucha ayuda. Yo vivo en plan vegano.

Las papeleras son una fuente inagotable de alimentos. La gente tira la comida todo el tiempo. Para los fines de la reunión urbana, los contenedores son
fundamentales. Los contenedores de cualquier tienda de comestibles suelen estar llenos de comida por la tarde. Presta atención a la razón por la que se
ha tirado algo. A veces la fecha de caducidad ha pasado. Compruebe si es realmente malo: huela y mire las latas para ver si la tapa se ha volado. A veces
sólo se rompe el paquete o se daña una lata de un palé entero. Las frutas y verduras pueden estar descoloridas, pero siguen siendo comestibles sin
problemas. A menudo, se pueden encontrar grandes cantidades de los mismos productos. Por eso es bueno estar en contacto con otros coleccionistas para 
poder compartir. ¿O qué otra cosa vas a hacer con cien litros de zumo de naranja? En cualquier caso, no olvides lavarte las manos antes de comer nada. 
También puedes ponerte guantes para hacer el contenedor, pero esto puede dar lugar a que otros coleccionistas se burlen de ti.

En algunos lugares es ilegal el uso de contenedores. ¡Pide! Los mejores productos suelen tirarse por la noche. Durante el día se puede buscar en los cubos
de basura normales. Mucha gente tira el bocadillo apenas comido: ¿por qué no íbamos a terminar la mitad del bocadillo de falafel?

Muchos -pero, por supuesto, no todos- los postcivilizados también roban comida. Sin embargo, por lo general, sólo de las grandes cadenas de supermercados.
Al mismo tiempo, las órdenes morales universales son una de las cosas que nos gusta dejar atrás con la civilización. Cada uno debe determinar sus propias
directrices éticas.

Por último, está la jardinería de guerrilla. Cultivar alimentos en todos los lugares posibles Aunque no te lo comas tú, los demás te lo agradecerán.

## Ropa

Ponte lo que quieras. Esto debería ser evidente, pero no lo es. La gente civilizada no se viste como los extras de las películas de Mad Max (lo cual es 
una pena). La gente civilizada se preocupa mucho por lo que los demás piensan de su ropa.

Pero no basta con pensar fuera de las normas. Si sólo piensas fuera de las normas, aún no estás pensando en todas las posibilidades. "Ser diferente sólo
por ser diferente es un enfoque equivocado. Sé diferente porque realmente quieres serlo. Y si no, ponte traje y corbata. Se da demasiada importancia a 
la ropa.

Si quieres que tu ropa se adapte al mundo civilizado, no debería ser difícil. La gente tira la ropa que se ajusta a este mundo todo el tiempo. Pero el 
atractivo de la estética de la reutilización y la reapropiación es fuerte: a menudo es lo que nos aleja de la civilización. Si no existiera la sociedad 
con sus convenciones, ¿qué te pondrías? ¿Qué aspecto tendría usted? ¿Y qué materiales hay para desarrollar su estilo?

El hilo dental puede utilizarse maravillosamente como hilo, al igual que el tendón. Y una vez que empiece a utilizar contenedores, nunca le faltarán
materiales con los que trabajar.

## Espacio para vivir
A veces es difícil encontrar alojamiento. Para la gente civilizada, poseer tierras es muy importante. Lo llaman "propiedad" y les gusta privatizarlo. 
En la mayoría de los países, la impudicia de poseer edificios vacíos e impedir que otros los utilicen es perfectamente legal.

Afortunadamente, a los poscivilizados no les importan mucho las leyes (aunque, por supuesto, sabemos que a otros sí). La okupación de edificios 
abandonados, en particular, tiene que ver más con la ética. La okupación es complicada y contextual y no hay dos situaciones iguales. Pero si se 
me permite ser impertinente y generalizar, hay dos tipos de okupas: los que hacen castillos maravillosos y fantásticos de los lugares en los que viven,
y los que mean en la esquina.

La vida postcivilizada no consiste en orinar todas las noches (aunque no es difícil elaborar tu propio alcohol), sino en liberarse y crear una forma de
vida más compleja, diversa y natural.

Algunas personas postcivilizadas encuentran un trozo de bosque y construyen allí una cabaña, normalmente con material reciclado y aplicado de forma
creativa. Otros optan por un compromiso. Compran o alquilan un espacio para vivir, normalmente compartiéndolo con muchas otras personas para mantener
los costes bajos. Se construyen cabañas en terrenos alquilados, se montan literas en habitaciones alquiladas, etc.

## ¡Un momento!

"¿No te estás convirtiendo en un parásito de la civilización al vivir de sus residuos? ¿No estás tan atado al mundo del consumo y dependes de él como 
los que compran bienes?"

Sí, si quieres ver las cosas de esa manera. Pero no estamos hablando de simple (¡y honorable!) parasitismo. Estamos hablando de la vida post-civilizada.
Esto se basa en vivir de los recursos disponibles. Hoy en día hay muchos. Después del colapso, las cosas serán diferentes.

## Especialización y comunidad

Ser independiente es maravilloso. Pero poder confiar en la gente es aún mejor. Como aprender a valerse por sí mismo: cultivar sus propios alimentos, 
recolectarlos y cocinarlos; reparar su propia ropa, herramientas y juguetes; adquirir conocimientos sobre salud, primeros auxilios y atención médica de
urgencia; aprender a luchar, al menos para hacer frente a la violencia cotidiana; ver cómo se toman las decisiones por consenso, etc. Hay mucho que hacer.

Pero no hay que dominar todo. A la gente le gusta discutir si la especialización es realmente tan maravillosa como dicen los civilizados, o tan opresiva 
como dicen los primitivistas. La gente rara vez piensa en el hecho de que la especialización y la comunalidad no tienen por qué ser opuestas.

Es bueno que todo el mundo sepa que el ajo en té ayuda contra los resfriados. Pero, ¿cuántas personas pueden conocer todas las plantas que tienen efectos
curativos? Eso requiere especialistas. Y mientras algunas personas se especializan en este campo, otras aprenden a fabricar antibióticos. No hay nada que
la civilización haya producido que no podamos aprender.

¿Yo personalmente? No voy a dedicar mi tiempo a aprender los métodos de permacultura más eficaces. ¡Pero ciertamente ayudaré con la cosecha!

## ¿Cómo puedo sobrevivir al colapso?

Lo primero que debes saber para sobrevivir al apocalipsis es esto: no sobrevivirás a él. No eres especial. Si todos mueren, tú mueres. Si no es por la 
crisis ecológica (personalmente espero que el calentamiento global se acelere inexorablemente), es por la creciente militarización de nuestra sociedad.

Hay algo que no puedo dejar de recalcar: No sirve de nada que huyas y te escondas en una pequeña y solitaria cabaña en algún lugar solo -o con cinco 
amigos, por cierto-. (A no ser que creas en los zombis.) Si sólo te retiras y esperas a que todo mejore, entonces eres un cobarde, y no muy inteligente. 
No se puede dejar todo el trabajo para el cambio social a otros. Es precisamente este tipo de cobardía, este individualismo, el que nos ha llevado a la 
situación en la que nos encontramos hoy. Si te quedas de brazos cruzados viendo cómo un ejército fascista toma el control, acabarás muriendo estés donde
estés. Si no se intenta construir una agricultura basada en la permacultura con otros, no hay posibilidad de sobrevivir. E incluso si consigues mantenerte
vivo durante un tiempo con otras dos personas, desearás no haberlo hecho, a más tardar cuando se te desgarre el talón de Aquiles y te des cuenta de que 
tu amigo no es cirujano.

Nos guste o no, los humanos somos criaturas sociales. Por tanto, nuestra mejor esperanza para seguir vivos y evolucionar es buscar soluciones colectivas.

Por supuesto, estar entre mucha gente en caso de colapso también conlleva peligros. El hambre lleva a la gente a hacer cosas terribles. Pero los escenarios
de la mayoría de las novelas apocalípticas no son realistas: sólo habrá una guerra de "bandas errantes" si dejamos que eso ocurra. En la civilización hay 
clases dirigentes y se nos dice que no podemos organizarnos. Pero esto es una tontería. Las formas de organización social no se oponen a las expresiones
individuales de la voluntad. El poder no es algo que sólo se utiliza contra nosotros. Todos tenemos poder, como individuos y especialmente como grupos. 
Por tanto, no hay razón para hacer la guerra a otras bandas como "banda itinerante". Más bien, podemos enseñar la permacultura, los métodos de curación 
y las formas de organización post-civilización a otras personas.

Tampoco hay razón para no empezar inmediatamente a vivir una vida basada en esos principios. Podemos unir fuerzas con nuestros vecinos, compartir nuestros
recursos, cultivar alimentos, desarrollar una cultura apasionante y defendernos de quienes quieren arrebatárnoslo todo.

¿Y quién sabe? Quizá el colapso de la civilización industrial se produzca antes de que el calentamiento global nos empuje cada vez más rápido hacia el 
abismo. Quizá las reservas de petróleo se agoten antes de que se extinga la mayor parte de las formas de vida de la Tierra. Y puede que incluso se llegue
al punto de que la gente se dé cuenta de que tiene que destruir la civilización para poder vivir. Pero, ¿qué ocurre entonces?

Dos cosas: la vuelta al salvajismo y a la comunidad.

## Reactivación de la naturaleza

Independientemente de todas las leyes, lo más importante hoy es arrancar el asfalto y ayudar a que vuelvan los bosques. Algunas carreteras pueden ser 
útiles, pero para nosotros, los poscivilizados, hay demasiado espacio que ha sido despojado de su carácter salvaje. Cada carretera que atraviesa un
bosque lo divide por la mitad. Esto es fácil de ver cuando nos detenemos en una carretera de este tipo y nos bajamos del coche. Sólo el borde de un 
bosque sano está densamente cubierto de vegetación. En el interior, hay una cantidad asombrosa de espacio.

En cualquier caso, la naturaleza volverá a ocupar el lugar que le corresponde. Pero tiene mucho sentido apoyar este proceso. La desertificación es un 
problema enorme y preocupante. Es el resultado de la actividad humana y comenzó miles de años antes de la Revolución Industrial. Incluso los programas 
de repoblación forestal bien pensados suelen dejar la tierra seca a toda prisa. Los estudios ecológicos demuestran cada vez más claramente que es mejor 
dejar que los bosques sigan su propio camino.

No soy el único que piensa que sólo la forestación intensiva puede evitar que las condiciones climáticas se descontrolen por completo. Pero eso significa
volver a dar una oportunidad a la naturaleza, por nuestra propia felicidad. Las ideas antropocéntricas -es decir, las que consideran a las personas y 
sus necesidades como la prioridad absoluta- forman parte del desastroso camino de la civilización.

La arrogancia con la que la gente trata a la naturaleza es increíble. Ha llevado, entre otras cosas, a que permitamos a las empresas del carbón eliminar 
literalmente cordilleras enteras (véase, por ejemplo, lo ocurrido en los Montes Apalaches). El hecho de que no nos resistamos a estos actos monstruosos 
sólo confirma lo domesticados y mansos que nos hemos vuelto.

Al igual que debemos restaurar la naturaleza salvaje de la tierra, debemos restaurar la naturaleza salvaje de nosotros mismos.

## Comunidad

Con el colapso, la mayor parte de la infraestructura social actual desaparecerá. Los gobernantes harán cualquier cosa para mantenerse en el poder. 
Pero si nos organizamos nosotros mismos y nuestras comunidades, simplemente hacemos innecesarios los gobiernos y las empresas.

La gente se acerca naturalmente en tiempos de crisis. (Sí, podemos discutir eternamente sobre lo que es y no es la naturaleza humana, ¡pero estos son 
mis textos!) En cuanto surgen grandes problemas, el aislamiento humano desaparece. Una parada de autobús puede servir de simple ejemplo: normalmente 
todo el mundo se queda allí esperando el autobús sin que nadie hable con los demás; sin embargo, en cuanto el autobús se retrasa diez minutos, todos 
son amigos.

Cuando el huracán Katrina destruyó gran parte de Nueva Orleans en 2005, la gente se organizó para conseguir alimentos. Cuando las fuerzas del gobierno
finalmente aparecieron después de unos días, estaban principalmente ocupadas disparando a la gente. Las organizaciones burocráticas de socorro estaban 
tan hinchadas y eran tan ineficaces que algunos miembros de la Guardia Nacional demostraron que conservaban su humanidad a pesar de sus uniformes: 
enviaron secretamente alimentos, medicinas, etc. al proyecto anarquista de socorro Common Ground Relief. Lo hicieron porque sabían que los anarquistas
eran capaces de llevar la ayuda a donde realmente se necesitaba.

Siempre se dice que sin gobierno nos mataríamos unos a otros. Pero el comportamiento más cruel del ser humano se produce cuando los gobiernos crean 
crisis utilizando la fuerza para imponer la ley y el orden o el statu quo civilizado. (Y no se deje impresionar por el ejemplo eternamente utilizado 
de Somalia. En Somalia no faltan gobiernos, el país está lleno de señores de la guerra).

Nuestro trabajo no es otro que ayudar a las comunidades humanas de base a crecer, al igual que ayudamos a los bosques a recuperar las plazas de 
aparcamiento de los centros comerciales. Tenemos que organizarnos localmente para satisfacer nuestras necesidades: Alimentación, agua, vivienda,
atención sanitaria y cultura. Y tenemos que luchar contra las reliquias de la civilización que intentan defender su poder.

La mayoría de los manuales de supervivencia se centran en las habilidades que necesitamos para la supervivencia individual: filtrar agua, almacenar 
alimentos, construir viviendas improvisadas. Pueden ser útiles y vale la pena tener estos libros al alcance de la mano. Lo mismo ocurre con lo que 
algunos llaman "OSG" (por "Oh shit gear"): Purificadores de agua potable, alimentos enlatados, mapas topográficos, botiquines de primeros auxilios 
con antibióticos y medicamentos recetados, gafas, máscaras de gas, filtros de aire, ropa impermeable.

Proyectos como la Aftershock Action Alliance de Nueva York preparan a los grupos de base para las catástrofes naturales. Trabajan a nivel local, 
en los barrios, y organizan talleres pertinentes.

Sólo juntos podemos luchar eficazmente contra el hambre, las enfermedades y los señores de la guerra. En eso es en lo que tenemos que centrarnos.

## La ciudad que no es una ciudad

¿Cómo llamamos a una ciudad que no es una ciudad?

No lo sé.

Pero la etiqueta que utilicemos no es realmente importante. Más interesante es la cuestión de cómo es una ciudad que no lo es.

La urbanización es uno de los principales rasgos que distinguen a la civilización de otras formas de convivencia social. Si se trata de dejar atrás la 
civilización (que es el tema principal de estos textos), hay que analizar el fenómeno de la urbanización.

Mi diccionario me dice que una ciudad es una "comunidad grande". Sin embargo, esto plantea la cuestión de qué se entiende por "municipio". A esto mi 
diccionario tiene la siguiente respuesta: "Un municipio es un área de asentamiento humano que tiene un nombre, límites claramente definidos y un gobierno". Esta explicación ilustra inmediatamente dónde están los problemas de las ciudades.

Lo del gobierno es fácil de descartar para mí. Soy anarquista. No creo en el Estado ni en los gobiernos. Me resisto a la idea de un aparato administrativo
central que tome todas las decisiones. Y me resisto a la idea de que no se me permita hacer más que elegir a una persona para que tome todas las decisiones por mí. Me interesa mucho más el autogobierno individual y colectivo. Supongo que conoces el viejo chiste: la democracia significa que dos ovejas y tres lobos deciden qué cenar. Bueno, al menos en los círculos en los que me muevo, esa es una broma popular.

Hay mucha literatura sobre el anarquismo, las "formas horizontales de organización" y cosas similares, así que no me detendré en eso aquí.

Pasemos en cambio al siguiente problema de la ciudad, que según la definición del New Oxford American Dictionary es "límites claramente definidos". 
Estos límites, en mi opinión, son una de las pruebas más claras del carácter patológico de la civilización. Una cordillera no tiene límites claramente
definidos; tiene un antepecho de colinas. Ni una tormenta tiene límites claramente definidos, ni mi género.

Las etiquetas pueden ser útiles como descripciones, pero eso no significa confinarse a sí mismo o a las regiones dentro de "límites claramente definidos".
Además de ser ilusoria (las fronteras son siempre más permeables de lo que creemos), conduce a todo tipo de horrores, como el nacionalismo. Pongamos un
ejemplo: Soy vegano. Utilizo el término porque es la forma más fácil de describir lo que como. Sin embargo, no me siento definido por el término. 
No tengo ningún sentimiento nacionalista en lo que respecta al veganismo. Por ejemplo, me importa poco lo que comas (o al menos bastante). Simplemente 
odio la agricultura industrial de animales y no quiero tener nada que ver con ella.

Así que las ciudades tienen gobiernos y límites claramente definidos. Bien. ¡Pero sin mí!

El teórico de la anticivilización Derrick Jensen ha definido las ciudades como lugares donde la gente vive en tal densidad que es necesaria una
importación permanente de recursos (ese es mi resumen). Jensen cree que esto funciona bien mientras no haya escasez y toda la gente tenga que comerciar.
Cuando no es así, surge el conflicto.

Para mí, esta consideración es el tercer argumento contra la ciudad tal y como la conocemos.

Sin embargo, no podemos abandonar por completo los modos de vida urbanos. Dada la población mundial actual, la Tierra quedaría completamente destruida 
si la gente no viviera en lugares densamente poblados.

Y hay algo más: para ser sincero, me gusta vivir en la ciudad. (O digamos: en muchas ciudades, ya que soy nómada.) Por supuesto, también me gusta lo 
salvaje, pero sólo puede seguir siendo salvaje si hay ciudades.

Las ciudades son un foco de la multiculturalidad que hace que nuestro mundo sea tan interesante. En las ciudades nacen las ideas. Las ciudades son el
lugar donde la gente se encuentra.

Francamente, no tengo mucho problema en que sigamos refiriéndonos a nuestros espacios urbanos como "ciudades" y sólo cambiemos el significado de la
palabra. Por supuesto, el mismo argumento podría aplicarse en relación con la palabra "civilización", pero mi opinión personal es que esta palabra está 
demasiado empapada de sangre para ser conservada. ¿Y es realmente necesario tener una palabra para la "sociedad más avanzada"? Personalmente, no me
interesa este pensamiento lineal del progreso. Pero bueno, ese es otro tema...

## Los que no son de la ciudad

Así que si no queremos un gobierno, unas fronteras claramente definidas y una importación permanente de recursos, tratemos de imaginar una ciudad sin 
estos aspectos: una ciudad más emocionante, útil y libre.

Cuanto más aprendo sobre las tribus (que son diferentes tanto de los grupos pequeños como de las civilizaciones), más atractivas me parecen como 
sistema social. Solía pensar que una tribu era una especie de familia extendida, un grupo homogéneo en el que se nace y del que sólo se puede escapar
mediante el matrimonio o el aislamiento. Pero resulta que estaba equivocado.

Según tengo entendido en la investigación antropológica, las tribus son heterogéneas y tienen fronteras abiertas. Las personas y las ideas se mueven
entre ellas de una forma que los Estados nación nunca permitirían.

Veo una ciudad sin gobierno ni límites claramente definidos como un espacio compartido por un gran número de tribus (o, si se quiere: culturas) 
superpuestas.

Al fin y al cabo, eso es lo que han sido siempre las ciudades bajo la fachada de homogeneidad impuesta por sus gobiernos: cambian de barrio en barrio,
de edificio en edificio, incluso de habitación en habitación. Mi imagen de Nueva York (o de Ámsterdam, o de cualquier otro lugar en el que haya vivido)
puede ser completamente diferente de la imagen que tienen otras personas de Nueva York, incluso si caminamos por las mismas calles: todo depende de los
círculos en los que nos movamos. Aparte del hecho de que compartimos algunas infraestructuras -el metro, por ejemplo-, bien podríamos vivir en ciudades
diferentes.

A lo largo de la historia se han experimentado formas de organización igualitarias en oposición a las jerarquías gubernamentales. Los éxitos fueron a 
menudo alentadores, pero el Estado siempre se impuso gracias a su despiadada violencia. Lo que tengo en mente es una federación de tribus (o culturas, 
o quizás sindicatos, si te gusta ese tipo de cosas) que toman colectivamente las decisiones para la población urbana.

A menudo me preguntan qué significaría una descentralización tan amplia para la especialización profesional y la ciencia compleja, por ejemplo, la 
exploración espacial. Mi respuesta es que todos tenemos diferentes prioridades. Las personas que quieren ir al espacio pueden hacer sus investigaciones
al respecto. Y si otros quieren apoyarlo, son libres de hacerlo.

Mi opinión personal es que será difícil convencerme de formas de exploración espacial ecológicamente sostenibles, pero supongo que nada es imposible. 
Hay, por cierto, una novela sobre esto. En Mi viaje con Aristóteles a la utopía anarquista, Graham Purchase habla de los pioneros espaciales de mentalidad
ecológica y sindicalista que trabajan en satélites fabricados con bioplásticos. ¿Loco? Por supuesto. Esto es válido para todas las ideas interesantes.

## La sostenibilidad

El motivo de lucro del capitalismo debe ser sustituido por un motivo de sostenibilidad. Conocemos la objeción común: "El capitalismo, el beneficio y 
el interés propio forman parte de la naturaleza humana". Sin embargo, podemos simplemente pedir a todos los que siguen argumentando esto que miren un 
poco más de cerca la investigación antropológica y biológica actual. Cada vez se da más importancia al papel de la cooperación en el proceso evolutivo.

Lo que en realidad pertenece a la "naturaleza humana" es el principio de sostenibilidad: nuestra supervivencia depende de que consigamos vivir en armonía 
con nuestro entorno. Es tan sencillo como eso.

Hacer realidad la sostenibilidad en los espacios urbanos es un reto especial. Pero se puede hacer. Sólo hay que ver lo que la gente ya ha conseguido en 
los ámbitos de la agricultura vertical, la hidroponía y la permacultura. La agricultura (más exactamente: el monocultivo) nos llevó originalmente a la 
locura de la civilización, pero estas técnicas innovadoras pueden sacarnos de ella.

¿Se pueden cultivar suficientes alimentos en las zonas urbanas para evitar la importación constante de recursos? Claro que sí. ¿Por qué no? Hay tejados
y miles y miles de habitaciones iluminadas por el sol. Además, hay superficies verticales casi inconmensurables.

Producir verduras y frutas es fácil. Los cereales y las proteínas son más difíciles, pero también lo conseguiremos.

Algunos aspectos de la vida sostenible son incluso más fáciles de establecer en zonas densamente pobladas. En la ciudad, por ejemplo, no todos los 
hogares necesitan su propio compost. Varios hogares pueden trabajar juntos. Los coches tampoco son necesarios, siempre que la planificación urbana sea
sensata (es decir, ecológica). Podemos caminar y utilizar formas sostenibles de transporte público.

## Lo salvaje de la ciudad
La ciudad puede ser tan salvaje como el bosque. Los edificios van y vienen en función de sus necesidades. Para ello no es necesaria la planificación
centralizada. Hay un desarrollo natural y la biodiversidad es enorme. La ciudad -o la no-ciudad o lo que sea- puede ser fácilmente el hogar de las
personas que han encontrado su camino de vuelta a la naturaleza: el hogar de los post-civilizados.

{{< vimeo 760675684>}}

